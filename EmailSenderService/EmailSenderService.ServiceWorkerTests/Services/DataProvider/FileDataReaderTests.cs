﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EmailSenderService.Common;
using EmailSenderService.ServiceWorker.Services.DataProvider;
using Moq;
using Serilog;
using Xunit;
using System.IO;

namespace EmailSenderService.ServiceWorkerTests.Services.DataProvider
{
    public class FileDataReaderTests
    {
        [Fact]
        public void GetCollection_FilePathExists_ReturnsCollection()
        {
            //Arrange
            int counter = 0;
            int toReturn = 10;
            Mock<IConfigurationContext> conf = new Mock<IConfigurationContext>();
            Mock<ILogger> logger = new Mock<ILogger>();
            FileDataReader dr = new FileDataReader(logger.Object, conf.Object);

            conf.Setup(x => x.ReadAmount).Returns(toReturn);
            logger.Setup(x => x.Error(It.IsAny<string>())).Callback(() => { });
            logger.Setup(x => x.Information(It.IsAny<string>())).Callback(() => { });

            //Act
            var result = dr.GetCollection<Person>("test.csv", ref counter);

            //Assert
            Assert.Equal(toReturn, counter);
            Assert.Equal(toReturn, result.Count);
        }

        [Fact]
        public void GetCollection_CounterBiggerThanDataInFile_ReturnsEmptyCollection()
        {
            //Arrange
            int counter = 10000;
            int toReturn = 10;
            Mock<IConfigurationContext> conf = new Mock<IConfigurationContext>();
            Mock<ILogger> logger = new Mock<ILogger>();
            FileDataReader dr = new FileDataReader(logger.Object, conf.Object);

            conf.Setup(x => x.ReadAmount).Returns(toReturn);
            logger.Setup(x => x.Error(It.IsAny<string>())).Callback(() => { });
            logger.Setup(x => x.Information(It.IsAny<string>())).Callback(() => { });

            //Act
            var result = dr.GetCollection<Person>("test.csv", ref counter);

            //Assert
            Assert.Empty(result);
        }

        [Fact]
        public void GetCollection_DirectoryNotExists_ThrowsDirectoryNotFoundException()
        {
            //Arrange
            int counter = 0;
            int toReturn = 10;
            Mock<IConfigurationContext> conf = new Mock<IConfigurationContext>();
            Mock<ILogger> logger = new Mock<ILogger>();
            FileDataReader dr = new FileDataReader(logger.Object, conf.Object);

            conf.Setup(x => x.ReadAmount).Returns(toReturn);
            logger.Setup(x => x.Error(It.IsAny<string>())).Callback(() => { });
            logger.Setup(x => x.Information(It.IsAny<string>())).Callback(() => { });

            //Act - Assert
            Assert.Throws(typeof(DirectoryNotFoundException), () => dr.GetCollection<Person>("somerandompath/test.csv", ref counter));
        }

        [Fact]
        public void GetCollection_FileNotFound_ThrowsFileNotFoundException()
        {
            //Arrange
            int counter = 0;
            int toReturn = 10;
            Mock<IConfigurationContext> conf = new Mock<IConfigurationContext>();
            Mock<ILogger> logger = new Mock<ILogger>();
            FileDataReader dr = new FileDataReader(logger.Object, conf.Object);

            conf.Setup(x => x.ReadAmount).Returns(toReturn);
            logger.Setup(x => x.Error(It.IsAny<string>())).Callback(() => { });
            logger.Setup(x => x.Information(It.IsAny<string>())).Callback(() => { });

            //Act - Assert
            Assert.Throws(typeof(FileNotFoundException), () => dr.GetCollection<Person>("fileNameNotFound.csv", ref counter));
        }

        [Fact]
        public void GetCollection_FilePathIsEmpty_ThrowsArgumentException()
        {
            //Arrange
            int counter = 0;
            int toReturn = 10;
            Mock<IConfigurationContext> conf = new Mock<IConfigurationContext>();
            Mock<ILogger> logger = new Mock<ILogger>();
            FileDataReader dr = new FileDataReader(logger.Object, conf.Object);

            conf.Setup(x => x.ReadAmount).Returns(toReturn);
            logger.Setup(x => x.Error(It.IsAny<string>())).Callback(() => { });
            logger.Setup(x => x.Information(It.IsAny<string>())).Callback(() => { });

            //Act - Assert
            Assert.Throws(typeof(ArgumentException), () => dr.GetCollection<Person>(string.Empty, ref counter));
        }

        [Fact]
        public void GetCollection_FilePathIsNull_ThrowsArgumentNullException()
        {
            //Arrange
            int counter = 0;
            int toReturn = 10;
            Mock<IConfigurationContext> conf = new Mock<IConfigurationContext>();
            Mock<ILogger> logger = new Mock<ILogger>();
            FileDataReader dr = new FileDataReader(logger.Object, conf.Object);

            conf.Setup(x => x.ReadAmount).Returns(toReturn);
            logger.Setup(x => x.Error(It.IsAny<string>())).Callback(() => { });
            logger.Setup(x => x.Information(It.IsAny<string>())).Callback(() => { });

            //Act - Assert
            Assert.Throws(typeof(ArgumentNullException), () => dr.GetCollection<Person>(null, ref counter));
        }
    }
}
