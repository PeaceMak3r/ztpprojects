﻿using System;
using EmailSenderService.Common;
using System.Configuration;
using Serilog;

namespace EmailSenderService
{
	public class ConfigurationContext : IConfigurationContext
	{
		private readonly ILogger _logger;

	    public string FilePath { get; }
	    public string EmailTemplatePath { get; }
	    public string ReadCronInterval { get; }
	    public int ReadAmount { get; }

	    public ConfigurationContext(ILogger logger)
		{
			try
			{
				_logger = logger;
				_logger.Information("[ConfigurationContext][ctor] Loading settings..");

				FilePath = ConfigurationManager.AppSettings["FilePath"];
				EmailTemplatePath = ConfigurationManager.AppSettings["EmailTemplatePath"];
				ReadAmount = int.Parse(ConfigurationManager.AppSettings["ReadAmount"]);
				ReadCronInterval = ConfigurationManager.AppSettings["ReadCronInterval"];

				_logger.Information("[ConfigurationContext][ctor] Settings loaded..");
			}
			catch (Exception ex)
			{
				_logger.Error(string.Format("[ConfigurationContext][ctor] EXCEPTION occured while loading settings.. {0}", ex));
				throw ex;
			}
		}
	}
}
