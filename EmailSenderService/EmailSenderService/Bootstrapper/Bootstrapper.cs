﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Autofac;
using Serilog;

namespace EmailSenderService
{
    public class Bootstrapper
    {
        public static IContainer Initialize(ContainerBuilder builder)
        {
            var filtered = FilterAssemblies();

            var assigned = filtered.Where(a => a.GetTypes().Any(x =>
                x.IsAssignableTo<Autofac.Module>() && x != typeof(Autofac.Module))).ToList();

            Log.Logger = new LoggerConfiguration()
                .WriteTo.RollingFile(string.Format($"{AppDomain.CurrentDomain.BaseDirectory}LOGS\\log.log"),
                    outputTemplate: "{Timestamp:yyyy-MM-dd HH:mm:ss.fff} [{Level}] {Message}{NewLine}{Exception}")
                .WriteTo.ColoredConsole()
                .CreateLogger();

            if (!assigned.Any())
                return builder.Build();

            foreach (var assembly in assigned)
            {
                builder.RegisterAssemblyModules(assembly);
            }

            builder.RegisterInstance(Log.Logger);

            builder.RegisterType<ConfigurationContext>()
                .AsImplementedInterfaces()
                .SingleInstance()
                .AutoActivate();

            return builder.Build();
        }

        private static Assembly[] FilterAssemblies()
        {
            var assemblies = Assembly.GetEntryAssembly().GetReferencedAssemblies(); // AppDomain.CurrentDomain.GetAssemblies();

            return assemblies.Select(assembly => Assembly.Load(assembly)).ToArray();
        }
    }
}
