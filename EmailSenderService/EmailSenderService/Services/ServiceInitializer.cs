﻿using Autofac;
using EmailSenderService.ServiceWorker;
using Topshelf;

namespace EmailSenderService
{
	public static class ServiceInitializer
	{
		public static void Configure()
		{
			var container = Bootstrapper.Initialize(new ContainerBuilder());

			HostFactory.Run(x =>
			{
				x.UseSerilog();
				x.Service<IScheduledJob>(s =>
				{
					s.ConstructUsing(name => container.Resolve<IScheduledJob>());
					s.WhenStarted(tc => tc.Start());
					s.WhenStopped(tc => tc.Stop());
				});

				x.RunAsLocalSystem();

				x.SetDescription("Email sender to received people");
				x.SetDisplayName("EmailSenderService");
				x.SetServiceName("EmailSenderService");
			});
		}
	}
}
