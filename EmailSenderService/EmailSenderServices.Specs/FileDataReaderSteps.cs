﻿using EmailSenderService.Common;
using EmailSenderService.ServiceWorker.Services.DataProvider;
using Moq;
using System;
using System.Collections.Generic;
using TechTalk.SpecFlow;
using Xunit;
using System.Linq;

namespace EmailSenderServices.Specs
{
    [Binding]
    public class FileDataReaderSteps
    {
        private string _path;
        private int _counter;
        IList<Person> _result;

        [Given(@"I have entered ""(.*)"" into the FileReader")]
        public void GivenIHaveEnteredIntoTheFileReader(string p0)
        {
            _path = $"{AppDomain.CurrentDomain.BaseDirectory}\\{p0}";
        }

        [Given(@"I have entered (.*) into the counter")]
        public void GivenIHaveEnteredIntoTheCounter(int p0)
        {
            _counter = p0;
        }

        [When(@"I press get")]
        public void WhenIPressGet()
        {
            Mock<IConfigurationContext> conf = new Mock<IConfigurationContext>();
            Mock<Serilog.ILogger> logger = new Mock<Serilog.ILogger>();

            conf.Setup(x => x.ReadAmount).Returns(2);
            logger.Setup(x => x.Information(It.IsAny<string>())).Callback(() => { });
            logger.Setup(x => x.Error(It.IsAny<string>())).Callback(() => { });


            var fileReader = new FileDataReader(logger.Object, conf.Object);

            _result = fileReader.GetCollection<Person>(_path, ref _counter);
        }

        [Then(@"the result should get following collection:")]
        public void ThenTheResultShouldGetFollowingCollection(Table table)
        {
            var result = true;
            for (int i = 0; i < table.Rows.Count; i++)
            {
                var row = table.Rows[i];

                if (!_result.Any(x => x.Email == row["Email"] && x.Name == row["Name"]))
                {
                    result = false;
                    break;
                }
            }

            Assert.True(result);
        }
    }
}
