﻿Feature: FileDataReader

@directoryExist
Scenario: Get Data Collection
    Given I have entered "test.csv" into the FileReader
	And I have entered 0 into the counter
	When I press get
	Then the result should get following collection:
	| Name | Email     |
	| y    | xx@xx.com |
	| z    | zz@zz.com |