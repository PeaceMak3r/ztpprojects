﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmailSenderService.Common
{
	public interface IConfigurationContext
	{
		string FilePath { get; }
		string EmailTemplatePath { get; }
		string ReadCronInterval { get; }
		int ReadAmount { get; }
	}
}
