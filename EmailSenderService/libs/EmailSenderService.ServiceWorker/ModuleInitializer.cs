﻿using Autofac;
using EmailSenderService.ServiceWorker.Services.DataProvider;
using Quartz.Spi;

namespace EmailSenderService.ServiceWorker
{
	public class ModuleInitializer : Module
	{
		protected override void Load(ContainerBuilder builder)
		{
			RegisterTypes(builder);
		}

		private void RegisterTypes(ContainerBuilder builder)
		{
			builder.RegisterType<ScheduledJob>()
				.AsImplementedInterfaces()
				.SingleInstance();

			builder.RegisterType<MailSender>()
				.AsImplementedInterfaces()
				.SingleInstance();

            builder.RegisterType<FileDataReader>()
                .AsImplementedInterfaces()
                .SingleInstance();

            builder.RegisterInstance(Quartz.Impl.StdSchedulerFactory.GetDefaultScheduler())
				.AsImplementedInterfaces()
				.SingleInstance();

			builder.RegisterType<JobInjectorHelper>()
				.As<IJobFactory>()
				.SingleInstance();

			builder.RegisterType<SendMailJobWorker>()
				.AsSelf()
				.InstancePerLifetimeScope();
		}
	}
}
