﻿using System;
using Autofac;
using Quartz;
using Quartz.Spi;

namespace EmailSenderService.ServiceWorker
{
	public class JobInjectorHelper : IJobFactory
	{
		private readonly ILifetimeScope _lifetimeScope;

		public JobInjectorHelper(ILifetimeScope lifetimeScope)
		{
			_lifetimeScope = lifetimeScope;
		}

		public IJob NewJob(TriggerFiredBundle bundle, IScheduler scheduler)
		{
			var jobDetail = bundle.JobDetail;

			Type jobType = jobDetail.JobType;

			var job = _lifetimeScope.Resolve(jobType);

			return (IJob)job;
		}

		public void ReturnJob(IJob job)
		{
		}
	}
}
