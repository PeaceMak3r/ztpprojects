﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EmailSenderService.Common;
using Serilog;

namespace EmailSenderService.ServiceWorker.Services.DataProvider
{
    public class FileDataReader: IDataProvider
    {
        private readonly ILogger _logger;
        private readonly IConfigurationContext _configuration;

        public FileDataReader(ILogger logger, IConfigurationContext configuration)
        {
            _logger = logger;
            _configuration = configuration;
        }

        public IList<T> GetCollection<T>(string filePath, ref int counter)
        {
            var result = new List<T>();
            try
            {
                _logger.Information("[QuartzWorker][GetPeople] Starting reading file...");
                using (var reader = new CsvHelper.CsvReader(new StreamReader(filePath)))
                {
                    result = reader.GetRecords<T>()
                        .Skip(counter)
                        .Take(_configuration.ReadAmount)
                        .ToList();

                    if (result.Count == 0)
                    {
                        _logger.Information(string.Format("[QuartzWorker][GetCollection] No new records available..."));
                        return result;
                    }

                    counter += result.Count;
                    _logger.Information(string.Format("[QuartzWorker][GetCollection] Loaded {0} people...", result.Count));
                }
            }
            catch (Exception ex)
            {
                _logger.Error(string.Format("[QuartzWorker][GetCollection] EXCEPTION during reading the file...{0}", ex));
                throw ex;
            }

            return result;
        }
    }
}
