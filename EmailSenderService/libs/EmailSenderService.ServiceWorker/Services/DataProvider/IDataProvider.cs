﻿using System.Collections.Generic;

namespace EmailSenderService.ServiceWorker.Services.DataProvider
{
    public interface IDataProvider
    {
        IList<T> GetCollection<T>(string filePath, ref int counter);
    }
}
