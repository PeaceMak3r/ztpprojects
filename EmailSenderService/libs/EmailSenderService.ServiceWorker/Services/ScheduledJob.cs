﻿using System;
using EmailSenderService.Common;
using Quartz;
using Quartz.Spi;
using Serilog;

namespace EmailSenderService.ServiceWorker
{
	public class ScheduledJob : IScheduledJob
	{
		private readonly IConfigurationContext _configuration;
		private readonly ILogger _logger;
		private readonly IScheduler _scheduler;

		public ScheduledJob(IScheduler scheduler, IConfigurationContext configuration, IJobFactory jobFactory, ILogger logger)
		{
			_logger = logger;
			_scheduler = scheduler;
			_configuration = configuration;

			_scheduler.JobFactory = jobFactory;
		}

		public void Start()
		{
			try
			{
				_logger.Information("[ServiceWorker][Start] Starting scheduler..");
				_scheduler.Start();

				IJobDetail job = JobBuilder.Create<SendMailJobWorker>()
					.WithIdentity("QuartzWorker", "QuartzWorkerGroup")
					.Build();

				ITrigger trigger = TriggerBuilder.Create()
					.WithIdentity("QuartzWorkerTrigger", "TriggerGroup")
					.StartNow()
					.WithCronSchedule(_configuration.ReadCronInterval)
					.Build();

				_scheduler.ScheduleJob(job, trigger);
				_logger.Information("[ServiceWorker][Start] Scheduler job registered. Running..");
			}
			catch (Exception ex)
			{
				_logger.Error(string.Format("[ServiceWorker][Start] EXCEPTION on firing job...{0}", ex));
				throw ex;
			}
		}

		public void Stop()
		{
			_scheduler.Shutdown();
			_logger.Information("[ServiceWorker][Stop] Scheduler stopped..");
		}
	}
}
