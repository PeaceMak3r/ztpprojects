﻿using System.Collections.Generic;
using EmailSenderService.Common;

namespace EmailSenderService.ServiceWorker
{
	public interface IMailSender
	{
		void SendEmail(ICollection<Person> people);
	}
}
