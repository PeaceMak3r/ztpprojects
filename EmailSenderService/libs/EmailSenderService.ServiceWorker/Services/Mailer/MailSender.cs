﻿using System;
using System.Collections.Generic;
using EmailSenderService.Common;
using FluentEmail;
using Serilog;

namespace EmailSenderService.ServiceWorker
{
	public class MailSender : IMailSender
	{
		private readonly IConfigurationContext _configuration;
		private readonly ILogger _logger;

		public MailSender(ILogger logger, IConfigurationContext configuration)
		{
			_logger = logger;
			_configuration = configuration;
		}

		public void SendEmail(ICollection<Person> people)
		{
			try
			{
				_logger.Information(string.Format("[MailSender][SendEmail] Trying to send mails to {0} users...", people.Count));
				foreach (var person in people)
				{
					SendEmail(person);
				}
				_logger.Information(string.Format("[MailSender][SendEmail] Mails sent successfully..."));
			}
			catch (Exception ex)
			{
				_logger.Error(string.Format("[MailSender][SendEmail] EXCEPTION during sending emails...{0}", ex));
			}
		}

		private void SendEmail(Person person)
		{
			var email = Email
				.From("mail@mail.com")
				.To(person.Email)
				.Subject("Hello there!")
				.UsingTemplateFromFile(_configuration.EmailTemplatePath, new { Name = person.Name, Email = person.Email });

			email.Send();
		}
	}
}
