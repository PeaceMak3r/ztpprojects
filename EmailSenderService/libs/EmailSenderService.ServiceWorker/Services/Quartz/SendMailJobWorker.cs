﻿using EmailSenderService.Common;
using EmailSenderService.ServiceWorker.Services.DataProvider;
using Quartz;

namespace EmailSenderService.ServiceWorker
{
	public class SendMailJobWorker : IJob
	{
		private readonly IMailSender _mailSender;
		private readonly IConfigurationContext _configuration;
	    private readonly IDataProvider _dataProvider;
	    private int _counter;

		public SendMailJobWorker(IMailSender mailSender, IConfigurationContext configuration, 
            IDataProvider dataProvider)
		{
			_mailSender = mailSender;
			_configuration = configuration;
            _dataProvider = dataProvider;
		}

		public void Execute(IJobExecutionContext context)
		{
			var people = _dataProvider.GetCollection<Person>(_configuration.FilePath, ref _counter);

			if (people.Count > 0)
			{
				_mailSender.SendEmail(people);
			}
		}
	}
}
