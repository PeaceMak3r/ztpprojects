﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmailSenderService.ServiceWorker
{
	public interface IScheduledJob
	{
		void Start();
		void Stop();
	}
}
