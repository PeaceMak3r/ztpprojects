﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EnduroTrailsAnalizer.Common;

namespace EnduroTrailsAnalizer.Provider
{
    public interface IGpxReader
    {
        IList<GpxPoint> GetWaypointsList(string filePath);
    }
}
