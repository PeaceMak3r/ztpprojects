﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using EnduroTrailsAnalizer.Common;
using Serilog;

namespace EnduroTrailsAnalizer.Provider
{
    public class GpxFileReader : IGpxReader
    {
        public IList<GpxPoint> GetWaypointsList(string filePath)
        {
            Log.Information("[GpxReader][GetWaypointsList] Loading file..");
            XDocument gpxFile = XDocument.Load(filePath);
            XNamespace gpxNamespace = XNamespace.Get("http://www.topografix.com/GPX/1/1");

            Log.Information("[GpxReader][GetWaypointsList] File loaded.. Converting..");
            return gpxFile.Descendants(gpxNamespace + "trkpt")
                .Select(waypoint => new GpxPoint
            {
                Lat = Convert.ToDouble(waypoint.Attribute("lat")?.Value.Replace('.', ',')),
                Lon = Convert.ToDouble(waypoint.Attribute("lon")?.Value.Replace('.', ',')),
                Ele = Convert.ToDouble(waypoint.Element(gpxNamespace + "ele")?.Value.Replace('.', ',')),
                Time = Convert.ToDateTime(waypoint.Element(gpxNamespace + "time")?.Value)
            }).ToList(); 
        }
    }
}
