﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace IoC
{
    internal class Container : IContainer
    {
        internal List<KeyValuePair<Type, Type>> TypeContainer { get; } = new List<KeyValuePair<Type, Type>>();
        public T Resolve<T>()
        {
            if (TypeContainer.All(v => v.Value != (typeof(T))))
            {
                throw new TypeNotFoundException();
            }

            var result = TypeContainer.First(v => v.Value == typeof(T));
            var resultType = result.Key;

            if (resultType == null)
            {
                throw new TargetTypeNotFoundException();
            }

            var parameters = resultType.GetConstructors()[0].GetParameters();
            List<object> typeParameters = new List<object>();

            foreach (var parameter in parameters)
            {
                if (TypeContainer.All(v => v.Value != parameter.ParameterType))
                {
                    throw new TypeNotFoundException();
                }
                var paramType = TypeContainer.First(v => v.Value == parameter.ParameterType).Value;
                var method = typeof(Container).GetMethod(nameof(Resolve));
                var outputObject = method.MakeGenericMethod(paramType).Invoke(this, null);
                typeParameters.Add(outputObject);
            }

            return typeParameters.Any()
                ? (T)Activator.CreateInstance(resultType, typeParameters.ToArray())
                : (T)Activator.CreateInstance(resultType);
        }
    }
}
