﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace IoC
{
    public class Builder : IBuilder
    {
        private readonly IList<RegisterableItem> _containerList = new List<RegisterableItem>();
        public IContainer Build()
        {
            var container = new Container();

            foreach (var item in _containerList)
            {
                if (container.TypeContainer.Any(c => item.AsType.Any(t => t == c.Key)))
                {
                    throw new TypeRegisteredException();
                }

                item.AsType.ForEach(t => container.TypeContainer.Add(new KeyValuePair<Type, Type>(item.InType, t)));
            }

            var assemblies = AppDomain.CurrentDomain.GetAssemblies()
                .Where(a => a.GetReferencedAssemblies().Any(r => r == Assembly.GetExecutingAssembly().GetName()))
                .ToList();

            return container;
        }

        public IRegisterableItem RegisterType<T>()
        {
            var newType = new RegisterableItem(typeof(T));
            _containerList.Add(newType);
            return newType;
        }
    }
}
