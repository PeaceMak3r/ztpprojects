﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IoC
{
    internal class RegisterableItem : IRegisterableItem
    {
        internal Type InType { get; }
        internal List<Type> AsType { get; } = new List<Type>();

        internal RegisterableItem(Type type)
        {
            InType = type;
        }
        public IRegisterableItem As<T>()
        {
            var asType = typeof(T);
            if (!asType.IsAssignableFrom(InType))
            {
                throw new NotAssignableFromException();
            }

            AsType.Add(asType);
            return this;
        }

        public IRegisterableItem AsSelf()
        {
            AsType.Add(InType);
            return this;
        }

        public IRegisterableItem AsImplementedInterfaces()
        {
            AsType.AddRange(InType.GetInterfaces());
            return this;
        }

        public IRegisterableItem SingleInstance()
        {
            // TODO:
            throw new NotImplementedException();
        }

        //TODO: autoactivate
    }
}
