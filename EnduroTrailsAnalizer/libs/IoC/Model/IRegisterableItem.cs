﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IoC
{
    public interface IRegisterableItem
    {
        IRegisterableItem As<T>();
        IRegisterableItem AsSelf();
        IRegisterableItem AsImplementedInterfaces();
        IRegisterableItem SingleInstance();
    }
}
