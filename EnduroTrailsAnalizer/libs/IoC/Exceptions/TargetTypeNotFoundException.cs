﻿using System;

namespace IoC
{
    internal class TargetTypeNotFoundException : Exception
    {
        internal TargetTypeNotFoundException() : base("Target type not found.")
        {
            
        }
    }
}
