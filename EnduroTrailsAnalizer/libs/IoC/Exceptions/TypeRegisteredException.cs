﻿using System;

namespace IoC
{
    internal class TypeRegisteredException : Exception
    {
        public TypeRegisteredException() : base("Type has already been registered.")
        {
        }
    }
}
