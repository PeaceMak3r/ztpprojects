﻿using System;

namespace IoC
{
    internal class TypeNotFoundException : Exception
    {
        internal TypeNotFoundException() : base("Could not found type.")
        {
        }
    }
}
