﻿using System;

namespace IoC
{
    internal class NotAssignableFromException : Exception
    {
        internal NotAssignableFromException() : base("Object not assignable from type.")
        {
        }
    }
}
