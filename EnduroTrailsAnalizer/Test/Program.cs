﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EnduroTrailsAnalizer;

namespace Test
{
    class Program
    {
        static void Main(string[] args)
        {
            ITrailsAnalizer trails = new TrailsAnalizer(ConfigurationManager.AppSettings["FilePath"]);

            StringBuilder sb = new StringBuilder("Twister:\n");
            sb.AppendFormat("Total distance: {0} km \n", trails.GetDistance());
            sb.AppendFormat("Climbing distance: {0} km \n", trails.GetClimbingDistance());
            sb.AppendFormat("Descent distance: {0} km \n", trails.GetDescentDistance());
            sb.AppendFormat("Flat distance: {0} km \n", trails.GetFlatDistance());
            sb.AppendFormat("Min speed: {0} km/h \n", trails.GetMinSpeed());
            sb.AppendFormat("Max speed: {0} km/h \n", trails.GetMaxSpeed());
            sb.AppendFormat("Avg speed: {0} km/h \n", trails.GetAvgSpeed());
            sb.AppendFormat("Min Elev: {0} m \n", trails.GetMinElev());
            sb.AppendFormat("Avg Elev: {0} m \n", trails.GetAvgElev());
            sb.AppendFormat("Total Climbing: {0} m \n", trails.GetTotalClimbing());
            sb.AppendFormat("Total Desc: {0} m \n", trails.GetTotalDescent());
            sb.AppendFormat("Final Balance: {0} m \n", trails.GetFinalBalance());
            sb.AppendFormat("Total Track Time: {0}\n", trails.GetTotalTrackTime());
            sb.AppendFormat("Climbing Time: {0}\n", trails.GetClimbingTime());
            sb.AppendFormat("Desc Time: {0} \n", trails.GetDescTime());
            sb.AppendFormat("Flat Time: {0} \n", trails.GetFlatTime());

            Console.WriteLine(sb);
            Console.ReadKey();
        }
    }
}
