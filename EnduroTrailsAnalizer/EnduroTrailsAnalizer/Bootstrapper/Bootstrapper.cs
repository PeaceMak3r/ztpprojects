﻿using EnduroTrailsAnalizer.Provider;
using IoC;
using Serilog;
using Serilog.Core;

namespace EnduroTrailsAnalizer
{
    public class Bootstrapper
    {
        internal static IContainer Container;
        public static void Initialize()
        {
            var builder = new Builder();
            RegisterTypes(builder);
            Container = builder.Build();
        }

        private static void RegisterTypes(IBuilder builder)
        {
            builder.RegisterType<GpxFileReader>()
                .AsImplementedInterfaces();
        }
    }
}
