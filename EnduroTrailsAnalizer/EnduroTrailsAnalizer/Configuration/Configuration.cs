﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Serilog;

namespace EnduroTrailsAnalizer
{
    public class Configuration
    {
        public static void Initialize()
        {
            Log.Logger = new LoggerConfiguration()
                .WriteTo.RollingFile("LOGS/log-{Date}.txt")
                .CreateLogger();

            Bootstrapper.Initialize();
        }
    }
}
