﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EnduroTrailsAnalizer
{
    public interface ITrailsAnalizer
    {
        double GetDistance();
        double GetClimbingDistance();
        double GetDescentDistance();
        double GetFlatDistance();
        double GetMinSpeed();
        double GetMaxSpeed();
        double GetAvgSpeed();
        double GetMinElev();
        double GetAvgElev();
        double GetTotalClimbing();
        double GetTotalDescent();
        double GetFinalBalance();
        TimeSpan GetTotalTrackTime();
        TimeSpan GetClimbingTime();
        TimeSpan GetDescTime();
        TimeSpan GetFlatTime();
    }
}
