﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EnduroTrailsAnalizer.Common;
using EnduroTrailsAnalizer.Provider;
using Serilog;

namespace EnduroTrailsAnalizer
{
    public class TrailsAnalizer : ITrailsAnalizer
    {
        private readonly IList<GpxPoint> _waypoints;
        private const double R = 6373;
        private const double D2R = (Math.PI / 180.0);

        public TrailsAnalizer(string filePath)
        {
            Configuration.Initialize();
            _waypoints = Bootstrapper.Container.Resolve<IGpxReader>().GetWaypointsList(filePath);
            Log.Information("[TrailsAnalizer] Points received.");
        }

        #region Distance
        public double GetDistance()
        {
            double distance = 0;
            Log.Information("[TrailsAnalizer][GetDistance] Calculating...");

            try
            {
                for (int i = 1; i < _waypoints.Count; i++)
                {
                    distance += GetDistance2Points(_waypoints[i], _waypoints[i - 1]);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "[TrailsAnalizer][GetDistance] Exception.");
            }

            return Math.Round(distance, 2);
        }

        public double GetClimbingDistance()
        {
            double distance = 0;
            Log.Information("[TrailsAnalizer][GetClimbingDistance] Calculating...");

            try
            {
                for (int i = 1; i < _waypoints.Count; i++)
                {
                    if (_waypoints[i].Ele > _waypoints[i - 1].Ele)
                    {
                        distance += GetDistance2Points(_waypoints[i], _waypoints[i - 1]);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "[TrailsAnalizer][GetClimbingDistance] Exception.");
            }

            return Math.Round(distance, 2);
        }

        public double GetDescentDistance()
        {
            double distance = 0;
            Log.Information("[TrailsAnalizer][GetDescentDistance] Calculating...");

            try
            {
                for (int i = 1; i < _waypoints.Count; i++)
                {
                    if (_waypoints[i].Ele < _waypoints[i - 1].Ele)
                    {
                        distance += GetDistance2Points(_waypoints[i], _waypoints[i - 1]);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "[TrailsAnalizer][GetDescentDistance] Exception.");
            }

            return Math.Round(distance, 2);
        }

        public double GetFlatDistance()
        {
            double distance = 0;
            Log.Information("[TrailsAnalizer][GetFlatDistance] Calculating...");

            try
            {
                for (int i = 1; i < _waypoints.Count; i++)
                {
                    if (_waypoints[i].Ele == _waypoints[i - 1].Ele)
                    {
                        distance += GetDistance2Points(_waypoints[i], _waypoints[i - 1]);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "[TrailsAnalizer][GetFlatDistance] Exception.");
            }

            return Math.Round(distance, 2);
        }
        #endregion

        #region Speed
        public double GetMinSpeed()
        {
            Log.Information("[TrailsAnalizer][GetMinSpeed] Calculating...");
            var waypointsAtDay = _waypoints
                .ToList()
                .GroupBy(x => x.Time.Date)
                .Select(x => new { x.Key, points = x.ToArray() });

            double minSpeed = 2000;

            try
            {
                foreach (var day in waypointsAtDay)
                {
                    for (int i = 1; i < day.points.Length; i++)
                    {
                        double t = (day.points[i].Time - day.points[i - 1].Time).TotalHours;
                        double s = GetDistance2Points(day.points[i - 1], day.points[i]);
                        double speed = s / t;

                        minSpeed = speed < minSpeed ? speed : minSpeed;
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "[TrailsAnalizer][GetMinSpeed] Exception.");
            }

            return Math.Round(minSpeed, 2);
        }

        public double GetMaxSpeed()
        {
            Log.Information("[TrailsAnalizer][GetMaxSpeed] Calculating...");
            var waypointsAtDay = _waypoints
                .ToList()
                .GroupBy(x => x.Time.Date)
                .Select(x => new { x.Key, points = x.ToArray() });

            double maxSpeed = 0;

            try
            {
                foreach (var day in waypointsAtDay)
                {
                    for (int i = 1; i < day.points.Length; i++)
                    {
                        double t = (day.points[i].Time - day.points[i - 1].Time).TotalHours;
                        double s = GetDistance2Points(day.points[i - 1], day.points[i]);
                        double speed = s / t;

                        maxSpeed = speed > maxSpeed ? speed : maxSpeed;
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "[TrailsAnalizer][GetMaxSpeed] Exception.");
            }

            return Math.Round(maxSpeed, 2);
        }

        public double GetAvgSpeed()
        {
            Log.Information("[TrailsAnalizer][GetAvgSpeed] Calculating...");
            var waypointsAtDay = _waypoints
                .ToList()
                .GroupBy(x => x.Time.Date)
                .Select(x => new { x.Key, points = x.ToArray() });

            double tempSpeed = 0;

            try
            {
                foreach (var day in waypointsAtDay)
                {
                    for (int i = 1; i < day.points.Length; i++)
                    {
                        double t = (day.points[i].Time - day.points[i - 1].Time).TotalHours;
                        double s = GetDistance2Points(day.points[i - 1], day.points[i]);
                        double speed = s / t;

                        tempSpeed += speed;
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "[TrailsAnalizer][GetAvgSpeed] Exception.");
            }

            return Math.Round(tempSpeed / (_waypoints.Count() - 1), 2);
        }

        #endregion

        #region Elevation

        public double GetMinElev()
        {
            Log.Information("[TrailsAnalizer][GetMinElev] Calculating...");
            return _waypoints.Min(x => x.Ele);
        }

        public double GetAvgElev()
        {
            Log.Information("[TrailsAnalizer][GetAvgElev] Calculating...");
            double totalElev = 0;

            try
            {
                foreach (var item in _waypoints)
                {
                    totalElev += item.Ele;
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "[TrailsAnalizer][GetAvgElev] Exception.");
            }

            return Math.Round(totalElev / _waypoints.Count, 2);
        }

        public double GetTotalClimbing()
        {
            Log.Information("[TrailsAnalizer][GetTotalClimbing] Calculating...");
            double totalClimbing = 0;

            try
            {
                for (int i = 1; i < _waypoints.Count(); i++)
                {
                    if (_waypoints[i].Ele > _waypoints[i - 1].Ele)
                    {
                        totalClimbing += (_waypoints[i].Ele - _waypoints[i - 1].Ele);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "[TrailsAnalizer][GetTotalClimbing] Exception.");
                throw;
            }

            return Math.Round(totalClimbing, 2);
        }

        public double GetTotalDescent()
        {
            Log.Information("[TrailsAnalizer][GetTotalDescent] Calculating...");
            double totalClimbing = 0;

            try
            {
                for (int i = 1; i < _waypoints.Count(); i++)
                {
                    if (_waypoints[i].Ele < _waypoints[i - 1].Ele)
                    {
                        totalClimbing += (_waypoints[i - 1].Ele - _waypoints[i].Ele);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "[TrailsAnalizer][GetTotalDescent] Exception.");
            }

            return totalClimbing;
        }

        public double GetFinalBalance()
        {
            Log.Information("[TrailsAnalizer][GetFinalBalance] Calculating...");
            return _waypoints.Max(x => x.Ele) - _waypoints.Min(x => x.Ele);
        }

        #endregion

        #region Time
        public TimeSpan GetTotalTrackTime()
        {
            Log.Information("[TrailsAnalizer][GetTotalTrackTime] Calculating...");
            return _waypoints.Max(x => x.Time) - _waypoints.Min(x => x.Time);
        }

        public TimeSpan GetClimbingTime()
        {
            Log.Information("[TrailsAnalizer][GetClimbingTime] Calculating...");
            TimeSpan climbingTime = TimeSpan.Zero;

            try
            {
                for (int i = 1; i < _waypoints.Count(); i++)
                {
                    if (_waypoints[i].Ele > _waypoints[i - 1].Ele)
                    {
                        climbingTime += (_waypoints[i].Time - _waypoints[i - 1].Time);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "[TrailsAnalizer][GetClimbingTime] Exception.");
            }

            return climbingTime;
        }

        public TimeSpan GetDescTime()
        {
            Log.Information("[TrailsAnalizer][GetDescTime] Calculating...");
            TimeSpan descTime = TimeSpan.Zero;

            try
            {
                for (int i = 1; i < _waypoints.Count(); i++)
                {
                    if (_waypoints[i].Ele < _waypoints[i - 1].Ele)
                    {
                        descTime += (_waypoints[i].Time - _waypoints[i - 1].Time);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "[TrailsAnalizer][GetDescTime] Exception.");
            }

            return descTime;
        }

        public TimeSpan GetFlatTime()
        {
            Log.Information("[TrailsAnalizer][GetFlatTime] Calculating...");
            TimeSpan flatTime = TimeSpan.Zero;

            try
            {
                for (int i = 1; i < _waypoints.Count(); i++)
                {
                    if (_waypoints[i].Ele == _waypoints[i - 1].Ele)
                    {
                        flatTime += (_waypoints[i].Time - _waypoints[i - 1].Time);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "[TrailsAnalizer][GetFlatTime] Exception.");
            }

            return flatTime;
        }
        #endregion

        #region private Methods
        private double GetDistance2Points(GpxPoint w1, GpxPoint w2)
        {
            double distance = 0;

            try
            {
                double dlon = (w2.Lon - w1.Lon) * D2R;
                double dlat = (w2.Lat - w1.Lat) * D2R;

                double a = Math.Pow((Math.Sin(dlat / 2)), 2) + Math.Cos(w1.Lat * D2R) * Math.Cos(w2.Lat) * Math.Pow(Math.Sin(dlon / 2), 2);
                double c = 2 * Math.Atan2(Math.Sqrt(a), Math.Sqrt(1 - a));
                distance += R * c;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "[TrailsAnalizer][GetDistance2Points] Exception.");
            }

            return Math.Round(distance, 5);
        }
        #endregion
    }
}
