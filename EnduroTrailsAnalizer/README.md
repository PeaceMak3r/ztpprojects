# EnduroTrailsAnalizer #

EnduroTrailsAnalizer is project which reads track data from file and calculates how much time did it take to ride it, speed, length of the track and others...

### Used technologies ###

* Visual Studio 2015 with Update 3
* Console application
* Serilog
* Custom IoC implemented