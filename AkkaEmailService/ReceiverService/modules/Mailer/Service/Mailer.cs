﻿using EmailService.Common;
using FluentEmail;
using System;

namespace Mailer.Service
{
    public class Mailer : IMailer
    {
        public void SendEmail(Person message)
        {
            var email = Email
                .From("mail@mail.com")
                .To(message.Email)
                .Subject("Hello there!")
                .UsingTemplateFromFile($"{AppDomain.CurrentDomain.BaseDirectory}\\template.html", 
                    new { Name = message.Name, Email = message.Email });

            email.Send();
        }
    }
}
