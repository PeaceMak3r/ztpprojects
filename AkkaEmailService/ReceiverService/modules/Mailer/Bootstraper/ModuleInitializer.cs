﻿using Autofac;

namespace Mailer.Bootstraper
{
    class ModuleInitializer : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType(typeof(Service.Mailer))
                .AsImplementedInterfaces()
                .SingleInstance();
        }
    }
}
