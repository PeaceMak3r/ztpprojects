﻿using System;
using System.IO;
using System.Linq;
using System.Reflection;
using Autofac;
using ServiceHostRunner;
using EmailService.Core;

namespace ReceiverService
{
    public class Bootstrapper
	{
		public static IContainer Initialize(ContainerBuilder builder)
		{
			var filtered = FilterAssemblies();

			var assigned = filtered.Where(a => a.GetTypes().Any(x =>
				x.IsAssignableTo<Autofac.Module>() && x != typeof(Autofac.Module))).ToList();

			if (!assigned.Any())
				return builder.Build();

			foreach (var assembly in assigned)
			{
				builder.RegisterAssemblyModules(assembly);
			}

            builder.Register(c => new ServiceHost<EmailReceiverService>(
                new EmailReceiverService
                {
                    ServiceName = "ReceiverService",
                    ServiceDescription = "Gets items from queue.",
                    ServiceDisplayName = "ReceiverService"
                }))
                .SingleInstance()
                .AsSelf();

		    builder.RegisterType<AutofacContainer>()
		        .AutoActivate();

			return builder.Build();
		}

		private static Assembly[] FilterAssemblies()
		{
			var referencedPaths = Directory.GetFiles(AppDomain.CurrentDomain.BaseDirectory, "*.dll");
			return referencedPaths
				.Select(path => AppDomain.CurrentDomain.Load(AssemblyName.GetAssemblyName(path)))
				.ToArray();
		}
	}
}
