﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EmailService.Common;

namespace ReceiverService
{
	public class Configuration : IConfigurationExt
	{
        public string EmailTemplatePath { get; set; }
        public string FolderPath { get; set; }

        public Configuration()
		{
        }
    }
}
