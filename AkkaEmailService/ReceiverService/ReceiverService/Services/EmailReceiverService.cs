﻿using ServiceHostRunner;
using Akka.Actor;
using Akka.Configuration;

namespace ReceiverService
{
    public class EmailReceiverService : IServiceBase
    {
        public string ServiceName { get; set; }
        public string ServiceDescription { get; set; }
        public string ServiceDisplayName { get; set; }

        public void Start()
        {
            var system = ActorSystem.Create("Receiver", ConfigurationFactory.Load());
        }

        public void Stop()
        {
        }
    }
}
