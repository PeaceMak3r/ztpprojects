﻿using Autofac;
using ServiceHostRunner;

namespace ReceiverService
{
    class Program
    {
        static void Main(string[] args)
        {
            var container = Bootstrapper.Initialize(new ContainerBuilder());
            var service = container.Resolve<ServiceHost<EmailReceiverService>>();
            service.Run();
        }
    }
}
