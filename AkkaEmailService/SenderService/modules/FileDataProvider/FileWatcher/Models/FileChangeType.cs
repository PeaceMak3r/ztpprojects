﻿namespace FileDataProvider
{
    public enum FileChangeType
    {
        Modified,
        Renamed,
        Created,
        Deleted
    }
}
