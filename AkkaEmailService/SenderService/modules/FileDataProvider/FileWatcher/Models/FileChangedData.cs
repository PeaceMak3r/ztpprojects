﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileDataProvider
{
    public class FileChangedData
    {
        public string FullPath { get; set; }
        public string Name { get; set; }
        public string OldName { get; set; }
        public FileChangeType Type { get; set; }
    }
}
