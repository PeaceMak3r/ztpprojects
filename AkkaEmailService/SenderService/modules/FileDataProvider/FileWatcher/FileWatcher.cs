﻿using EmailService.Common;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System;
using System.Reactive.Disposables;
using System.Reactive.Linq;

namespace FileDataProvider
{
    public class FileWatcher : IFileWatcher
    {
        private readonly FileSystemWatcher _fileWatcher;
        private readonly IList<IObserver<FileChangedData>> _observers;

        public FileWatcher(IConfiguration configuration)
        {
            _observers = new List<IObserver<FileChangedData>>();

            _fileWatcher = new FileSystemWatcher()
            {
                Path = configuration.FolderPath,
                NotifyFilter = NotifyFilters.FileName |
                                NotifyFilters.LastWrite |
                                NotifyFilters.LastAccess |
                                NotifyFilters.DirectoryName,
                Filter = "*.csv"
            };

            _fileWatcher.Changed += _fileWatcher_Changed;
            _fileWatcher.Created += _fileWatcher_Created;
            _fileWatcher.Deleted += _fileWatcher_Deleted;
            _fileWatcher.Renamed += _fileWatcher_Renamed;

            _fileWatcher.EnableRaisingEvents = true;
        }

        private void _fileWatcher_Renamed(object sender, RenamedEventArgs e)
        {
            foreach (var observer in _observers)
            {
                observer.OnNext(new FileChangedData
                {
                    Name = e.Name,
                    OldName = e.OldName,
                    Type = FileChangeType.Renamed
                });
            }
        }

        private void _fileWatcher_Deleted(object sender, FileSystemEventArgs e)
        {
            foreach (var observer in _observers)
            {
                observer.OnNext(new FileChangedData
                {
                    Name = e.Name,
                    Type = FileChangeType.Deleted
                });
            }
        }

        private void _fileWatcher_Created(object sender, FileSystemEventArgs e)
        {
            foreach (var observer in _observers)
            {
                observer.OnNext(new FileChangedData
                {
                    FullPath = e.FullPath,
                    Name = e.Name,
                    OldName = string.Empty,
                    Type = FileChangeType.Created
                });
            }
        }

        private void _fileWatcher_Changed(object sender, FileSystemEventArgs e)
        {
            foreach (var observer in _observers)
            {
                observer.OnNext(new FileChangedData
                {
                    FullPath = e.FullPath,
                    Name = e.Name,
                    OldName = string.Empty,
                    Type = FileChangeType.Modified
                });
            }
        }

        public IDisposable Subscribe(IObserver<FileChangedData> observer)
        {
            _observers.Add(observer);
            return Disposable.Create(() =>
            {
                _observers.Remove(observer);
            });
        }
    }
}
