﻿using EmailService.Common;
using System.Configuration;

namespace SenderService
{
    public class Configuration : IConfiguration
    {
        public string FolderPath { get; }

        public Configuration()
        {
            FolderPath = ConfigurationManager.AppSettings["FolderPath"];
        }
    }
}
