﻿using Autofac;
using ServiceHostRunner;

namespace SenderService
{
    class Program
    {
        static void Main(string[] args)
        {
            var container = Bootstrapper.Initialize(new ContainerBuilder());
            var service = container.Resolve<PersonEmailSender>();
            var host = new ServiceHost<PersonEmailSender>(service);
            host.Run();
        }
    }
}
