﻿using System;
using System.Linq;
using System.Reactive.Linq;
using System.Threading.Tasks;
using ServiceHostRunner;
using FileDataProvider;
using Akka.Actor;
using Akka.Routing;
using EmailService.Core;
using EmailService.Common;

namespace SenderService
{
    public class PersonEmailSender : IServiceBase
    {
        private IDisposable _repositoryObserver;
        private IActorRef _actor;
        private readonly IMessageRepository<Person> _messageRepository;

        public string ServiceName { get; set; }
        public string ServiceDescription { get; set; }
        public string ServiceDisplayName { get; set; }

        public PersonEmailSender(IMessageRepository<Person> messageRepository)
        {
            _messageRepository = messageRepository;
        }

        public void Start()
        {
            var system = ActorSystem.Create("SenderSystem", Akka.Configuration.ConfigurationFactory.Load());
            var props = Props.Create<PersonActor>()
                .WithRouter(FromConfig.Instance);

            _actor = system.ActorOf(props, nameof(PersonActor));

            _repositoryObserver = _messageRepository
                    .Where(x => x.Any())
                    .Subscribe(x =>
                    {
                        x.ToList()
                         .ForEach(e => _actor.Tell(new PersonMessage(e.Email, e.Name)));
                    });
        }

        public void Stop()
        {
            _messageRepository.Dispose();
            _repositoryObserver.Dispose();
        }
    }
}
