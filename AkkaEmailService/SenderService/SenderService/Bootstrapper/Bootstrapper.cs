﻿using System;
using System.IO;
using System.Linq;
using System.Reflection;
using Autofac;
using EmailService.Common;
using FileDataProvider;

namespace SenderService
{
    public class Bootstrapper
    {
        public static IContainer Initialize(ContainerBuilder builder)
        {
            var filtered = FilterAssemblies();

            var assigned = filtered.Where(a => a.GetTypes().Any(x =>
                x.IsAssignableTo<Autofac.Module>() && x != typeof(Autofac.Module))).ToList();

            if (!assigned.Any())
                return builder.Build();

            foreach (var assembly in assigned)
            {
                builder.RegisterAssemblyModules(assembly);
            }

            builder.Register(c => new PersonEmailSender(c.Resolve<IMessageRepository<Person>>())
                {
                    ServiceDescription = "Sender",
                    ServiceDisplayName = "ServiceSender",
                    ServiceName = "EmailSenderService"
                })
                .AsSelf()
                .SingleInstance();

            builder.RegisterType<Configuration>()
                .AsImplementedInterfaces()
                .SingleInstance()
                .AutoActivate();

            return builder.Build();
        }

        private static Assembly[] FilterAssemblies()
        {
            var referencedPaths = Directory.GetFiles(AppDomain.CurrentDomain.BaseDirectory, "*.dll");
            return referencedPaths
                .Select(path => AppDomain.CurrentDomain.Load(AssemblyName.GetAssemblyName(path)))
                .ToArray();
        }
    }
}
