# AkkaEmailServive #

AkkaEmailServive is project which reads data from csv files (name, email) and then sends emails to read email addresses. 

### Used technologies ###

* Visual Studio 2015 with Update 3
* Window Service
* Topshelf
* FluentEmail
* Akka.NET
* Akka.NET Remote
* Autofac