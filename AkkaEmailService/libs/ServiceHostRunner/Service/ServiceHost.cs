﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Topshelf;

namespace ServiceHostRunner
{
    public class ServiceHost<T>
		where T: class, IServiceBase
    {
	    private readonly T _service;

	    public ServiceHost(T service)
	    {
		    _service = service;
	    }

	    public void Run()
	    {
			HostFactory.Run(x =>
			{
				x.Service<IServiceBase>(s =>
				{
					s.ConstructUsing(name => _service);
					s.WhenStarted(tc => tc.Start());
					s.WhenStopped(tc => tc.Stop());
				});
				x.RunAsLocalSystem();

				x.SetDescription(_service.ServiceDescription);
				x.SetDisplayName(_service.ServiceDisplayName);
				x.SetServiceName(_service.ServiceName);
			});
		}
    }
}
