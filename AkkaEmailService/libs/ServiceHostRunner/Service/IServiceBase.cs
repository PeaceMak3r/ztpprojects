﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServiceHostRunner
{
	public interface IServiceBase
	{
		void Start();
		void Stop();
		string ServiceName { get; }
		string ServiceDescription { get; }
		string ServiceDisplayName { get; }
	}
}
