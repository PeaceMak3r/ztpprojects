﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmailService.Common
{
    public class Person : IBaseMessage
    {
        public string Email { get; set; }
        public string Name { get; set; }

        public int CompareTo(object obj)
        {
            var person = obj as Person;

            if (person == null)
            {
                return 0;
            }

            if (person.Name.Equals(Name) && person.Email.Equals(Email))
            {
                return 1;
            }

            return 0;
        }
    }
}
