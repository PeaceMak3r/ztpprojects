﻿namespace EmailService.Common
{
	public interface IConfiguration
	{
		string FolderPath { get; }
	}
}
