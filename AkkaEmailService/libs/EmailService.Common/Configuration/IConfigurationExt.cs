﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmailService.Common
{
    public interface IConfigurationExt : IConfiguration
    {
        string EmailTemplatePath { get; set; }
    }
}
