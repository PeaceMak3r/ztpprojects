﻿
namespace EmailService.Common
{
    public interface IMailer
    {
        void SendEmail(Person message);
    }
}
