﻿using Autofac;

namespace EmailService.Core
{
    public class AutofacContainer
    {
        internal static ILifetimeScope Container;

        public AutofacContainer(ILifetimeScope container)
        {
            Container = container;
        }
    }
}
