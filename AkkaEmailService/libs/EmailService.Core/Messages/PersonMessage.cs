﻿using EmailService.Common;

namespace EmailService.Core
{
    public class PersonMessage : IBaseMessage
    {
        public string Email { get; }
        public string Name { get; }

        public PersonMessage(string email, string name)
        {
            Email = email;
            Name = name;
        }

        public int CompareTo(object obj)
        {
            var person = obj as PersonMessage;

            if (person == null)
            {
                return 0;
            }

            if (person.Name.Equals(Name) && person.Email.Equals(Email))
            {
                 return 1;
            }

            return 0;
        }
    }
}
