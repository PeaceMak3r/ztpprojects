﻿using Akka.Actor;
using EmailService.Common;
using System;
using System.Threading.Tasks;
using Autofac;

namespace EmailService.Core
{
    public class PersonActor : ReceiveActor
    {
        private readonly IMailer _mailer;

        public PersonActor()
        {
            _mailer = AutofacContainer.Container.Resolve<IMailer>();

            Receive<PersonMessage>(x => HandlePersonMessage(x));
        }

        private void HandlePersonMessage(PersonMessage message)
        {
            if (AutofacContainer.Container == null)
                throw new ArgumentNullException(nameof(AutofacContainer.Container));

            _mailer.SendEmail(new Person
            {
                Email = message.Email,
                Name = message.Name
            });
        }
    }
}
