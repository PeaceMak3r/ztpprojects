﻿using Autofac;
using System;
using System.Linq;
using System.Reflection;

namespace PizzaStore.WebService
{
    public static class Bootstrapper
    {
        public static void Initialize(ContainerBuilder builder)
        {
            var filtered = FilterAssemblies();

            var assigned = filtered
                    .Where(a => a.GetTypes()
                    .Any(x => x.IsAssignableTo<Autofac.Module>() && x != typeof(Autofac.Module)))
                    .ToList();

            if (!assigned.Any())
                return;

            foreach (var assembly in assigned)
            {
                builder.RegisterAssemblyModules(assembly);
            }

            builder.RegisterType<ConfigurationContext>()
                .AsImplementedInterfaces()
                .SingleInstance()
                .AutoActivate();
        }

        private static Assembly[] FilterAssemblies()
        {
            var assemblies = AppDomain.CurrentDomain.GetAssemblies();

            return assemblies.Select(assembly => Assembly.Load(assembly.FullName)).ToArray();
        }
    }
}