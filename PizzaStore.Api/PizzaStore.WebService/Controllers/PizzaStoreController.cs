﻿using System.Collections.Generic;
using System.Linq;
using PizzaStore.Contract;
using PizzaStore.COR;
using System.Web.Http;

namespace PizzaStore.WebService
{
    public class PizzaStoreController : ApiController
    {
        private readonly DesiredHandler _desired;

        public PizzaStoreController(DesiredHandler desired, EnglishHandler english, PolishHandler polish, AnyHandler any)
        {
            _desired = desired;
            _desired.SetSuccessor(english);
            english.SetSuccessor(polish);
            polish.SetSuccessor(any);
        }

        [HttpGet]
        [Route("api/{id}/{language}")]
        public PizzaDto GetPizza(string id, string language)
        {
            return _desired.HandleWork(language, id);
        }

        [HttpGet]
        [Route("api/pizzas/{language}")]
        public List<PizzaDto> GetPizzasList(string language)
        {
            return _desired.HandleWork(language).ToList();
        }
    }
}
