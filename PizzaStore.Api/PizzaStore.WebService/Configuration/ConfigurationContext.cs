﻿using PizzaStore.Common;
using System;
using System.Web.Configuration;

namespace PizzaStore.WebService
{
    public class ConfigurationContext : IConfigurationContext
    {
        public string ConnectionString { get; }
        public string DbName { get; }

        public ConfigurationContext()
        {
            try
            {
                ConnectionString = WebConfigurationManager.AppSettings["ConnectionString"];
                DbName = WebConfigurationManager.AppSettings["DbName"];
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}