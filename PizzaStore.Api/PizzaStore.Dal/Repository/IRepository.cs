﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace PizzaStore.Dal
{
    public interface IRepository<T> where T : class
    {
        ICollection<T> Get(Expression<Func<T, bool>> query);
        IQueryable<T> GetAll();
        void Add(T entity);
        void Delete(Expression<Func<T, bool>> queryExpression);
        void Update(Expression<Func<T, bool>> queryExpression, T entity);
    }
}
