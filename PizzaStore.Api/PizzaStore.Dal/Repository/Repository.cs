﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace PizzaStore.Dal
{
    public class Repository<T> : IRepository<T> 
        where T : class
    {
        private readonly IMongoCollection<T> _collection;

        public Repository(IDbFactory factory)
        {
            _collection = factory.GetDb().GetCollection<T>(typeof(T).Name);
        }

        public ICollection<T> Get(Expression<Func<T, bool>> query)
        {
            return _collection.Find(query).ToList();
        }

        public IQueryable<T> GetAll() => _collection.AsQueryable();

        public void Add(T entity) => _collection.InsertOne(entity);

        public void Delete(Expression<Func<T, bool>> queryExpression)
        {
            _collection.DeleteOne(queryExpression);
        }

        public void Update(Expression<Func<T, bool>> queryExpression, T entity)
        {
            _collection.ReplaceOne(queryExpression, entity);
        }
    }
}