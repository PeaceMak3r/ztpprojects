﻿using MongoDB.Driver;
using PizzaStore.Common;

namespace PizzaStore.Dal
{
    public class DbFactory : IDbFactory
    {
        private readonly IMongoDatabase _db;

        public DbFactory(IConfigurationContext configuration)
        {
            var client = new MongoClient(configuration.ConnectionString);
            _db = client.GetDatabase(configuration.DbName);
        }

        public IMongoDatabase GetDb()
        {
            return _db;
        }
    }
}
