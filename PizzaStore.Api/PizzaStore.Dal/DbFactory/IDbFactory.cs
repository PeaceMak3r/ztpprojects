﻿using MongoDB.Driver;

namespace PizzaStore.Dal
{
    public interface IDbFactory
    {
        IMongoDatabase GetDb();
    }
}
