﻿using Autofac;

namespace PizzaStore.Dal
{
    public class ModuleInitializer : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<DbFactory>()
                .AsImplementedInterfaces()
                .SingleInstance()
                .AutoActivate();

            builder.RegisterGeneric(typeof(Repository<>))
                .AsImplementedInterfaces()
                .InstancePerDependency();
        }
    }
}
