﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System.Collections.Generic;

namespace PizzaStore.Dal
{
    public class Pizza
    {
        [BsonId]
        [BsonElement("_id")]
        public ObjectId _Id { get; set; }
        public double Price { get; set; }
        public string ImageUrl { get; set; }
        public List<PizzaContent> Contents { get; set; }
    }
}
