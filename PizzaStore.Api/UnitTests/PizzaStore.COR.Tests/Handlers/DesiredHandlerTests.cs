﻿using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;
using Moq;
using PizzaStore.Contract;
using PizzaStore.Dal;

namespace PizzaStore.COR.Tests.Handlers
{
    public class DesiredHandlerTests
    {
        [Fact]
        public void HandleWork_LanguageIsMatchToPatternAndExists_ReturnsCollection()
        {
            //arrange
            string language = "en-GB";
            Mock<IRepository<Pizza>> pizza = new Mock<IRepository<Pizza>>();
            List<Pizza> pizzas = new List<Pizza>
            {
                new Pizza
                {
                    Contents = new List<PizzaContent>
                    {
                        new PizzaContent
                        {
                            LanguageCode = language
                        }
                    }
                }
            };
            pizza.Setup(s => s.GetAll()).Returns(new EnumerableQuery<Pizza>(pizzas));
            BaseHandler handler = new DesiredHandler(pizza.Object);
            Mock<BaseHandler> successorHandler = new Mock<BaseHandler>(pizza.Object);
            successorHandler.Setup(s => s.HandleWork(It.IsAny<string>())).Returns(new EnumerableQuery<PizzaDto>(new List<PizzaDto>()));
            handler.SetSuccessor(successorHandler.Object);

            //act
            var result = handler.HandleWork(language);

            //assert
            successorHandler.Verify(x => x.HandleWork(language), Times.Never);
            Assert.NotEmpty(result);
        }

        [Fact]
        public void HandleWork_LanguageIsMatchToPatternButNotExists_CallsSuccessor()
        {
            //arrange
            string language = "en-GB";
            string desiredLanguage = "zh-CN";
            Mock<IRepository<Pizza>> pizza = new Mock<IRepository<Pizza>>();
            List<Pizza> pizzas = new List<Pizza>
            {
                new Pizza
                {
                    Contents = new List<PizzaContent>
                    {
                        new PizzaContent
                        {
                            LanguageCode = language
                        }
                    }
                }
            };
            pizza.Setup(s => s.GetAll()).Returns(new EnumerableQuery<Pizza>(pizzas));
            BaseHandler handler = new DesiredHandler(pizza.Object);
            Mock<BaseHandler> successorHandler = new Mock<BaseHandler>(pizza.Object);
            successorHandler.Setup(s => s.HandleWork(It.IsAny<string>())).Returns(new EnumerableQuery<PizzaDto>(new List<PizzaDto>()));
            handler.SetSuccessor(successorHandler.Object);

            //act
            var result = handler.HandleWork(desiredLanguage);

            //assert
            successorHandler.Verify(x => x.HandleWork(desiredLanguage), Times.Once);
            Assert.NotNull(result);
        }

        [Fact]
        public void HandleWork_LanguageDoesNotMatchPattern_ThrowsArgumentException()
        {
            //arrange
            string language = "bad format";
            Mock<IRepository<Pizza>> pizza = new Mock<IRepository<Pizza>>();
            BaseHandler handler = new DesiredHandler(pizza.Object);
            Mock<BaseHandler> successorHandler = new Mock<BaseHandler>(pizza.Object);
            successorHandler.Setup(s => s.HandleWork(It.IsAny<string>())).Returns(new EnumerableQuery<PizzaDto>(new List<PizzaDto>()));
            handler.SetSuccessor(successorHandler.Object);

            //act
            Exception ex = Assert.Throws<ArgumentException>(() => handler.HandleWork(language));

            //assert
            successorHandler.Verify(x => x.HandleWork(language), Times.Never);
            Assert.NotNull(ex);
        }
    }
}