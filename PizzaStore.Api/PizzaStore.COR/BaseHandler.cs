﻿using System.Collections.Generic;
using PizzaStore.Contract;
using PizzaStore.Dal;

namespace PizzaStore.COR
{
    public abstract class BaseHandler
    {
        protected BaseHandler Successor;
        protected readonly IRepository<Pizza> Pizza;

        public BaseHandler(IRepository<Pizza> pizza)
        {
            Pizza = pizza;
        }

        public void SetSuccessor(BaseHandler successor)
        {
            Successor = successor;
        }

        public abstract PizzaDto HandleWork(string language, string id);
        public abstract IEnumerable<PizzaDto> HandleWork(string language);
    }
}
