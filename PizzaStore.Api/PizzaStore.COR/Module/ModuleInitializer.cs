﻿using Autofac;

namespace PizzaStore.COR
{
    public class ModuleInitializer : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<DesiredHandler>()
                .AsSelf()
                .SingleInstance();

            builder.RegisterType<EnglishHandler>()
                .AsSelf()
                .SingleInstance();

            builder.RegisterType<PolishHandler>()
                .AsSelf()
                .SingleInstance();

            builder.RegisterType<AnyHandler>()
                .AsSelf()
                .SingleInstance();
        }
    }
}
