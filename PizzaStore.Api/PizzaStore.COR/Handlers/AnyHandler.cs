﻿using System.Collections.Generic;
using System.Linq;
using PizzaStore.Contract;
using MongoDB.Bson;
using PizzaStore.Dal;

namespace PizzaStore.COR
{
    public class AnyHandler : BaseHandler
    {
        public AnyHandler(IRepository<Pizza> pizza) : base(pizza)
        {
        }

        public override PizzaDto HandleWork(string language, string id)
        {
            var result = Pizza.GetAll().GetPizzaWithAnyLanguage(new ObjectId(id));

            return result ?? Successor?.HandleWork(string.Empty, id);
        }

        public override IEnumerable<PizzaDto> HandleWork(string language)
        {
            var result = Pizza.GetAll().GetPizzasWithAnyLanguage().ToList();

            if (!result.Any())
                Successor?.HandleWork(language);

            return result;
        }
    }
}
