﻿using System;
using System.Collections.Generic;
using System.Linq;
using PizzaStore.Contract;
using System.Text.RegularExpressions;
using MongoDB.Bson;
using PizzaStore.Dal;

namespace PizzaStore.COR
{
    public class DesiredHandler : BaseHandler
    {
        public DesiredHandler(IRepository<Pizza> pizza) : base(pizza)
        {
        }

        public override PizzaDto HandleWork(string language, string id)
        {
            if (!Regex.IsMatch(language, "[a-zA-Z]{2}-[A-Z]{2}"))
                throw new ArgumentException(nameof(language));

            var result = Pizza.GetAll().GetPizzaWithLanguage(language, new ObjectId(id));

            return result ?? Successor?.HandleWork(language, id);
        }

        public override IEnumerable<PizzaDto> HandleWork(string language)
        {
            if (!Regex.IsMatch(language, "[a-zA-Z]{2}-[A-Z]{2}"))
                throw new ArgumentException(nameof(language));

            var result = Pizza.GetAll().GetPizzasWithLanguage(language).ToList();

            return result.Any() ? result : Successor?.HandleWork(language);
        }
    }
}
