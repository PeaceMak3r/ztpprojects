﻿using System.Collections.Generic;
using System.Linq;
using PizzaStore.Contract;
using MongoDB.Bson;
using PizzaStore.Dal;

namespace PizzaStore.COR
{
    public class EnglishHandler : BaseHandler
    {
        public EnglishHandler(IRepository<Pizza> pizza) : base(pizza)
        {
        }

        public override PizzaDto HandleWork(string language, string id)
        {
            if (!language.Equals("en-GB"))
                return Successor?.HandleWork(language, id);

            var result = Pizza.GetAll().GetPizzaWithLanguage(language, new ObjectId(id));

            return result ?? Successor?.HandleWork(language, id);
        }

        public override IEnumerable<PizzaDto> HandleWork(string language)
        {
            if (!language.Equals("en-GB"))
                return Successor?.HandleWork(language);

            var result = Pizza.GetAll().GetPizzasWithLanguage(language).ToList();

            return result.Any() ? result : Successor?.HandleWork(language);
        }
    }
}
