﻿namespace PizzaStore.Common
{
    public interface IConfigurationContext
    {
        string ConnectionString { get; }
        string DbName { get; }
    }
}
