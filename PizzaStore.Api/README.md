# PizzaStoreApi #

PizzaStoreApi is web api which allows you to get pizza in selected language if available. 

### Used technologies ###

* Visual Studio 2015 with Update 3
* WebApi
* Autofac
* Chain of Responsibilty pattern
* MongoDb
* UnitTests
* Xunit
* Moq