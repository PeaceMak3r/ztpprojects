﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileDataProvider
{
    public interface IMessageRepository<TMessageType> : IObservable<IList<TMessageType>>, IObserver<FileChangedData>
    {
    }
}
