﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Disposables;
using EmailService.Common;

namespace FileDataProvider
{
    public class MessageRepository<TMessageType> : IMessageRepository<TMessageType>
        where TMessageType : class, IBaseMessage
    {
        private readonly IDictionary<string, IList<TMessageType>> _messageDictionary;
        private readonly IDataProvider<TMessageType> _provider;

        private readonly IList<IObserver<IList<TMessageType>>> _observers;
        private readonly IDisposable _fileObserver;

        public MessageRepository(IFileWatcher fileWatcher, IDataProvider<TMessageType> provider)
        {
            _provider = provider;
            _messageDictionary = new ConcurrentDictionary<string, IList<TMessageType>>();
            _observers = new List<IObserver<IList<TMessageType>>>();

            _fileObserver = fileWatcher
                .Subscribe(OnNext, onCompleted: OnCompleted, onError: OnError);
        }

        private void AddOrUpdate(string key, IList<TMessageType> value)
        {
            _messageDictionary[key] = value;
        }

        private void Remove(string key)
        {
            _messageDictionary.Remove(key);
        }

        private void Rename(string oldKey, string newKey)
        {
            var value = _messageDictionary[oldKey];
            _messageDictionary.Remove(oldKey);
            _messageDictionary[newKey] = value;
        }

        public void OnNext(FileChangedData value)
        {
            switch (value.Type)
            {
                case eFileChangeType.Modified:
                    var collection = _provider.GetDataCollection(value.FullPath);
                    IList<TMessageType> difference = null;
                    if (_messageDictionary.ContainsKey(value.Name))
                    {
                        difference = collection
                            .Where(x => _messageDictionary[value.Name].All(v => x.CompareTo(v).Equals(0)))
                            .ToList();
                        
                        collection = collection.Concat(difference).ToList();
                    }
                    _observers.ToList().ForEach(o => o.OnNext(difference ?? collection));
                    AddOrUpdate(value.Name, collection);
                    break;
                case eFileChangeType.Renamed:
                    Rename(value.OldName, value.Name);
                    break;
                case eFileChangeType.Created:
                    var values = _provider.GetDataCollection(value.FullPath);
                    _observers.ToList().ForEach(o => o.OnNext(values));
                    AddOrUpdate(value.Name, values);
                    break;
                case eFileChangeType.Deleted:
                    Remove(value.Name);
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        public void OnError(Exception error)
        {
        }

        public void OnCompleted()
        {
            return;
        }

        public IDisposable Subscribe(IObserver<IList<TMessageType>> observer)
        {
            _observers.Add(observer);
            return Disposable.Create(() =>
            {
                _observers.Remove(observer);
            });
        }

        //TODO: use this method to load files that are already in a folder 
        //      but after the main part of application subscribes
        private void InitLoad()
        {
            //string[] files = Directory.GetFiles(_fileWatcher.Path, "*.csv", SearchOption.AllDirectories);
            //foreach (var filePath in files)
            //{
            //    var fileName = Path.GetFileName(filePath);
            //    var collection = _provider.GetDataCollection(filePath);

            //    _repository.AddOrUpdate(fileName, collection);
            //}
        }
    }
}
