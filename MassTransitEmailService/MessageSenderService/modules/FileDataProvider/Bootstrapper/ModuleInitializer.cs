﻿using Autofac;

namespace FileDataProvider.Bootstrapper
{
	public class ModuleInitializer : Module
	{
		protected override void Load(ContainerBuilder builder)
		{
			builder.RegisterGeneric(typeof(CsvDataReader<>))
				.AsImplementedInterfaces()
				.SingleInstance();

            builder.RegisterGeneric(typeof(MessageRepository<>))
                .As(typeof(IMessageRepository<>))
                .SingleInstance();

		    builder.RegisterType<FileWatcher>()
		        .AsImplementedInterfaces()
		        .SingleInstance();
		}
	}
}
