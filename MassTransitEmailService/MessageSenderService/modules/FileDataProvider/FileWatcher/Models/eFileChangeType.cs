﻿namespace FileDataProvider
{
    public enum eFileChangeType
    {
        Modified,
        Renamed,
        Created,
        Deleted
    }
}
