﻿using System;
using System.Collections.Generic;

namespace FileDataProvider
{
    public interface IFileWatcher : IObservable<FileChangedData>
    {
    }
}