﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using CsvHelper;
using EmailService.Common;
using System;

namespace FileDataProvider
{
    public class CsvDataReader<T> : IDataProvider<T>
        where T : class
    {
        public IList<T> GetDataCollection(string path)
        {
            int counter = 0;
            while (counter < 3)
            {
                if (IsFileAccessable(path))
                {
                    using (var reader = new CsvReader(new StreamReader(path)))
                    {
                        var result = reader.GetRecords<T>()
                            .ToList();

                        return result;
                    }
                }
                counter++;
                System.Threading.Thread.Sleep(500);
            }
            return null;
        }

        private bool IsFileAccessable(string filePath)
        {
            FileStream stream = null;
            try
            {
                using (stream = File.Open(filePath, FileMode.Open, FileAccess.ReadWrite, FileShare.None))
                {
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
