﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using CsvHelper;
using EmailService.Common;

namespace FileDataProvider
{
	public class CsvDataProvider<T> : IDataProvider<T>
        where T: class
	{
		public IList<T> GetDataCollection(string path)
		{
			using (var reader = new CsvReader(new StreamReader(path)))
			{
				var result = reader.GetRecords<T>()
					.ToList();

				return result;
			}
		}
	}
}
