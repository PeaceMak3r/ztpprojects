﻿using Autofac;
using EmailService.Common;
using FileDataProvider;
using MessageManagementService;
using ServiceHostRunner;

namespace MessageSenderService
{
	class Program
	{
		static void Main(string[] args)
		{
			var container = Bootstrapper.Initialize(new ContainerBuilder());
		    var service = 
                new ServiceHost<EmailSenderService<Person>>(container.Resolve<EmailSenderService<Person>>());
			service.Run();
		}
	}
}
