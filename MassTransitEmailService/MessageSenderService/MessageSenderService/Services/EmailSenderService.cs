﻿using System;
using System.Linq;
using System.Reactive.Linq;
using System.Threading.Tasks;
using EmailService.Common;
using ServiceHostRunner;
using MessageManagementService;
using FileDataProvider;

namespace MessageSenderService
{
    public class EmailSenderService<TMessage> : IServiceBase
        where TMessage : class, IBaseMessage
    {
        private readonly ISender<TMessage> _sender;
        private readonly IDisposable _repositoryObserver;

        public string ServiceName { get; set; }
        public string ServiceDescription { get; set; }
        public string ServiceDisplayName { get; set; }

        public EmailSenderService(IMessageRepository<TMessage> messageRepository, ISender<TMessage> sender)
        {
            _sender = sender;

            _repositoryObserver = messageRepository
                    .Where(x => x.Any())
                    .Subscribe(x =>
                    {
                        Task.Factory.StartNew(() => _sender.SendAsync(x));
                    });
        }

        public void Start()
        {
            _sender.Start();
        }

        public void Stop()
        {
            _repositoryObserver.Dispose();
            _sender.Stop();
        }
    }
}
