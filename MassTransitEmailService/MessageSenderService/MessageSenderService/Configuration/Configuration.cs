﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EmailService.Common;

namespace MessageSenderService
{
	public class Configuration : IConfiguration
	{
		public string UserName { get; }
		public string Password { get; }
		public string RabbitMqUrl { get; }
		public string RabbitQueueName { get; }
		public string FolderPath { get; }

		public Configuration()
		{
			UserName = ConfigurationManager.AppSettings["UserName"];
			Password = ConfigurationManager.AppSettings["Password"];
			RabbitMqUrl = ConfigurationManager.AppSettings["RabbitMqUrl"];
			RabbitQueueName = ConfigurationManager.AppSettings["RabbitQueueName"];
			FolderPath = ConfigurationManager.AppSettings["FolderPath"];
		}
	}
}
