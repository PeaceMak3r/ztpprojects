﻿using EmailService.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mailer.Service
{
    public interface IMailer
    {
        void SendEmail(IPerson message);
    }
}
