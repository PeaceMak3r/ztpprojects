﻿using EmailService.Common;
using FluentEmail;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mailer.Service
{
    public class Mailer : IMailer
    {
        private readonly IConfigurationExt _configuration;

        public Mailer(IConfigurationExt configuration)
        {
            _configuration = configuration;
        }

        public void SendEmail(IPerson message)
        {
            var email = Email
                .From("mail@mail.com")
                .To(message.Email)
                .Subject("Hello there!")
                .UsingTemplateFromFile(_configuration.EmailTemplatePath, new { Name = message.Name, Email = message.Email });

            email.Send();
        }
    }
}
