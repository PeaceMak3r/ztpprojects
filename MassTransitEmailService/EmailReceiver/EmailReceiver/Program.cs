﻿using Autofac;
using ServiceHostRunner;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmailReceiver
{
	class Program
	{
		static void Main(string[] args)
		{
            var container = Bootstrapper.Initialize(new ContainerBuilder());
            var service = container.Resolve<ServiceHost<EmailReceiverService>>();
            service.Run();
        }
	}
}
