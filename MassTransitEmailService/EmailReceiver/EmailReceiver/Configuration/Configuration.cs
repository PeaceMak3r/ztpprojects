﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EmailService.Common;

namespace EmailReceiver
{
	public class Configuration : IConfigurationExt
	{
		public string UserName { get; }
		public string Password { get; }
		public string RabbitMqUrl { get; }
		public string RabbitQueueName { get; }
		public string FilePath { get; }
        public string EmailTemplatePath { get; set; }

        public Configuration()
		{
			UserName = ConfigurationManager.AppSettings["UserName"];
			Password = ConfigurationManager.AppSettings["Password"];
			RabbitMqUrl = ConfigurationManager.AppSettings["RabbitMqUrl"];
			RabbitQueueName = ConfigurationManager.AppSettings["RabbitQueueName"];
			FilePath = ConfigurationManager.AppSettings["FilePath"];
            EmailTemplatePath = ConfigurationManager.AppSettings["EmailTemplatePath"];
        }
    }
}
