﻿using EmailService.Common;
using ServiceHostRunner;
using Mailer.Service;
using MessageManagementService;
using System.Reactive.Linq;
using System;

namespace EmailReceiver
{
    public class EmailReceiverService : IServiceBase
    {
        private readonly IMailer _mailer;
        private readonly IReceiver<Person> _receiver;
        private readonly IDisposable _source;

        public string ServiceName { get; set; }
        public string ServiceDescription { get; set; }
        public string ServiceDisplayName { get; set; }

        public EmailReceiverService(IMailer mailer, IReceiver<Person> receiver)
        {
            _mailer = mailer;
            _receiver = receiver;
            _source = _receiver
                .Subscribe(onNext: x =>
                {
                    _mailer.SendEmail(x);
                });
        }

        public void Start()
        {
            _receiver.Start();
        }

        public void Stop()
        {
            _source.Dispose();
            _receiver.Stop();
        }
    }
}
