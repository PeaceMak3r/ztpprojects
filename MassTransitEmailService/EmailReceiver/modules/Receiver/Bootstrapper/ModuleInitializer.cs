﻿using Autofac;

namespace Receiver.Bootstrapper
{
    class ModuleInitializer : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterGeneric(typeof(Service.Receiver<>))
                .AsImplementedInterfaces()
                .InstancePerDependency();
        }
    }
}
