﻿using System;
using EmailService.Common;
using MassTransit;
using System.Threading.Tasks;
using Mailer.Service;
using Receiver.Service.Interface;

namespace Receiver.Consumers
{
    public class MessageConsumer<TMessageType> : IConsumer<TMessageType>
        where TMessageType: class, IBaseMessage
    {
        public EventHandler<TMessageType> MessageHandler { get; set; }

        public Task Consume(ConsumeContext<TMessageType> context)
        {
            MessageHandler?.Invoke(this, context.Message);
            return Task.FromResult(0);
        }
    }
}
