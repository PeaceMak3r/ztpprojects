﻿using EmailService.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Receiver.Service.Interface
{
    public interface IReceiver<TMessageType>
        where TMessageType: IBaseMessage
    {
        event EventHandler<TMessageType> MessageEvent;
        void Start();
        void Stop();
    }
}
