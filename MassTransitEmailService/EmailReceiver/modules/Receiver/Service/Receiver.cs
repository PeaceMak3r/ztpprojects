﻿using System;
using EmailService.Common;
using MassTransit;
using Receiver.Service.Interface;
using ServiceBusProvider;
using Receiver.Consumers;

namespace Receiver.Service
{
    public class Receiver<TMessageType> : IReceiver<TMessageType>
        where TMessageType: class, IBaseMessage
    {
        private readonly IBusControl _bus;
        private MessageConsumer<TMessageType> _consumer;

        public event EventHandler<TMessageType> MessageEvent;

        public Receiver(IServiceBusFactory factory, IConfiguration configuration)
        {
            _bus = factory.ConfigureBus(c =>
            {
                c.UseRateLimit(100, TimeSpan.FromSeconds(60));
                _consumer = new MessageConsumer<TMessageType>();
                _consumer.MessageHandler += ConsumerMessageHandler;
                c.Consumer(() => _consumer);
            });
        }

        private void ConsumerMessageHandler(object sender, TMessageType args)
        {
            MessageEvent?.Invoke(this, args);
        }

        public void Start()
        {
            _bus.Start();
        }

        public void Stop()
        {
            _consumer.MessageHandler -= ConsumerMessageHandler;
            _bus.Stop();
        }
    }
}
