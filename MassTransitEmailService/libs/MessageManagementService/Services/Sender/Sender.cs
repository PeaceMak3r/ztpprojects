﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using EmailService.Common;
using MassTransit;
using ServiceBusProvider;

namespace MessageManagementService
{
    public class Sender<TMessageType> : ISender<TMessageType>
        where TMessageType: class, IBaseMessage
    {
	    private readonly IBusControl _bus;
	    private readonly string _sendEnpoint;

	    public Sender(IServiceBusFactory factory, IConfiguration configuration)
	    {
		    _bus = factory.ConfigureBus();
		    _sendEnpoint = $"{configuration.RabbitMqUrl}{configuration.RabbitQueueName}";
	    }

        public void Start()
        {
            _bus.Start();
        }

        public void Stop()
        {
            _bus.Stop();
        }

        public async Task SendAsync(IEnumerable<TMessageType> collection)
	    {
		    var endpoint = _bus.GetSendEndpoint(new Uri(_sendEnpoint)).Result;

			foreach (var message in collection)
			{
			    await endpoint.Send(message);
			}
	    }
    }
}
