﻿using System.Collections.Generic;
using System.Threading.Tasks;
using EmailService.Common;

namespace MessageManagementService
{
	public interface ISender<TMessageType>
        where TMessageType: class, IBaseMessage
    {
	    void Start();
	    void Stop();
        Task SendAsync(IEnumerable<TMessageType> collection);
	}
}
