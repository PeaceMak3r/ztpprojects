﻿using System;
using EmailService.Common;
using ServiceBusProvider;
using MassTransit;
using System.Reactive.Disposables;
using System.Collections.Generic;
using System.Reactive.Linq;

namespace MessageManagementService
{
    public class Receiver<TMessageType> : IReceiver<TMessageType>
        where TMessageType : class, IBaseMessage
    {
        private readonly IBusControl _bus;
        private readonly IDisposable _source;
        private readonly MessageConsumer<TMessageType> _consumer = new MessageConsumer<TMessageType>();
        private readonly List<IObserver<TMessageType>> _observers = new List<IObserver<TMessageType>>();

        public Receiver(IServiceBusFactory factory, IConfiguration configuration)
        {
            _source = _consumer
                .Subscribe(this);

            _bus = factory.ConfigureBus(c =>
            {
                c.UseRateLimit(100, TimeSpan.FromSeconds(60));
                c.Consumer(() => _consumer);
            });
        }

        public void Start()
        {
            _bus.Start();
        }

        public void Stop()
        {
            _source.Dispose();
            _bus.Stop();
        }

        public void OnNext(TMessageType value)
        {
            _observers.ForEach(o => o.OnNext(value));
        }

        public void OnError(Exception error)
        {
        }

        public void OnCompleted()
        {
        }

        public IDisposable Subscribe(IObserver<TMessageType> observer)
        {
            _observers.Add(observer);
            return Disposable.Create(() =>
            {
                _observers.Remove(observer);
            });
        }
    }
}
