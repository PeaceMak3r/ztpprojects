﻿using EmailService.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MessageManagementService
{
    public interface IReceiver<TMessageType> : IObservable<TMessageType>, IObserver<TMessageType>
        where TMessageType: IBaseMessage
    {
        void Start();
        void Stop();
    }
}
