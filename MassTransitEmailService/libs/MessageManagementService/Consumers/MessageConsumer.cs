﻿using System;
using EmailService.Common;
using MassTransit;
using System.Threading.Tasks;
using System.Reactive.Disposables;
using System.Collections.Generic;

namespace MessageManagementService
{
    public class MessageConsumer<TMessageType> : IConsumer<TMessageType>, IObservable<TMessageType>
        where TMessageType: class, IBaseMessage
    {
        private readonly List<IObserver<TMessageType>> _observers = new List<IObserver<TMessageType>>();

        public Task Consume(ConsumeContext<TMessageType> context)
        {
            _observers.ForEach(o => o.OnNext(context.Message));
            return Task.FromResult(0);
        }

        public IDisposable Subscribe(IObserver<TMessageType> observer)
        {
            _observers.Add(observer);
            return Disposable.Create(() =>
            {
                _observers.Remove(observer);
            });
        }
    }
}
