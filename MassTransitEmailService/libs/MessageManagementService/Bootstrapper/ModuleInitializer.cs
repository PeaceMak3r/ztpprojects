﻿using Autofac;

namespace MessageManagementService
{
	public class ModuleInitializer : Module
	{
		protected override void Load(ContainerBuilder builder)
		{
			builder.RegisterGeneric(typeof(Sender<>))
				.AsImplementedInterfaces()
				.InstancePerDependency();
            builder.RegisterGeneric(typeof(Receiver<>))
                .AsImplementedInterfaces()
                .InstancePerDependency();
        }
	}
}
