﻿using Autofac;

namespace ServiceBusProvider.Bootstrapper
{
	public class ModuleInitializer : Module
	{
		protected override void Load(ContainerBuilder builder)
		{
			builder.RegisterType<ServiceBusFactory>()
				.AsImplementedInterfaces()
				.SingleInstance();
		}
	}
}
