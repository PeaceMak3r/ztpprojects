﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MassTransit;

namespace ServiceBusProvider
{
	public interface IServiceBusFactory
	{
		IBusControl ConfigureBus(Action<IReceiveEndpointConfigurator> config = null);
	}
}
