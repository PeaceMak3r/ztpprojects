﻿using System;
using EmailService.Common;
using MassTransit;

namespace ServiceBusProvider
{
    public class ServiceBusFactory : IServiceBusFactory
    {
        private readonly IConfiguration _configuration;

        public ServiceBusFactory(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public IBusControl ConfigureBus(Action<IReceiveEndpointConfigurator> config = null)
        {
            return Bus.Factory.CreateUsingRabbitMq(cfg =>
            {
                cfg.Host(new Uri(_configuration.RabbitMqUrl), h =>
                {
                    h.Username(_configuration.UserName);
                    h.Password(_configuration.Password);
                });

                if (config != null)
                    cfg.ReceiveEndpoint(_configuration.RabbitQueueName, config);
            });
        }
    }
}
