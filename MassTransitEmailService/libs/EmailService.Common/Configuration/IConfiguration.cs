﻿namespace EmailService.Common
{
	public interface IConfiguration
	{
		string UserName { get; }
		string Password { get; }
		string RabbitMqUrl { get; }
		string RabbitQueueName { get; }
		string FolderPath { get; }
	}
}
