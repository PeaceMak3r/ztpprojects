﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmailService.Common
{
	public interface IPerson : IBaseMessage
	{
		string Email { get; set; }
		string Name { get; set; }
	}
}
