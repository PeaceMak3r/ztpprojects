﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmailService.Common
{
	public interface IDataProvider<T>
        where T: class
	{
		IList<T> GetDataCollection(string path);
	}
}
